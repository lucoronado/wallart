from django.db import models

class Category (models.Model):
    idCategory = models.ForeignKey(max_length=30)
    nameCategory = models.CharField(max_length=30)
